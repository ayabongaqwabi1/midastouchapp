import React, { Component} from "react";
import "./app.css";

class App extends Component{
  constructor(){
    super();
    this.onB = this.onB.bind(this);
  }

  onB(){
    alert('Super Greetings!')
  }

  render(){
    return(
      <div className="App">
        <h1> Hello, World! </h1>
        <button style={{padding:"10px 20px", background: "orange", borderRadius: "5px"}} onClick={this.onB}>Say Hello</button>
        <p>In the jungle the mighty jungle the lion sleeps tonight ....</p>
      </div>
    );
  }
}

export default App;